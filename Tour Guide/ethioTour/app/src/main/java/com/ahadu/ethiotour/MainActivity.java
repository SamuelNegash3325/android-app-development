package com.ahadu.ethiotour;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
//    ViewPager viewPager;
//    PlaceViewPagerAdapter placeViewPagerAdapter;
//    List<PlaceModel> placeModels;
//    FloatingActionButton lan_btn;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        loadLocale();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
////        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);

//
//        placeModels = new ArrayList<>();
//        placeModels.add(new PlaceModel(R.mipmap.axum,getResources().getString(R.string.Axum),""));
//        placeModels.add(new PlaceModel(R.mipmap.denakil,getResources().getString(R.string.Ert_Ale),""));
//        placeModels.add(new PlaceModel(R.mipmap.fasil,getResources().getString(R.string.Fasil),""));
//        placeModels.add(new PlaceModel(R.mipmap.harar,getResources().getString(R.string.Harar),""));
//        placeModels.add(new PlaceModel(R.mipmap.hawassa,getResources().getString(R.string.Hawassa),""));
//        placeModels.add(new PlaceModel(R.mipmap.gambela,getResources().getString(R.string.Gambela),""));
//        placeModels.add(new PlaceModel(R.mipmap.lalibela,getResources().getString(R.string.Lalibela),""));
//        placeModels.add(new PlaceModel(R.mipmap.land,getResources().getString(R.string.Land),""));
//        placeModels.add(new PlaceModel(R.mipmap.tana,getResources().getString(R.string.Lake_Tana),""));
//        placeViewPagerAdapter = new PlaceViewPagerAdapter(placeModels,this);
//
//        lan_btn = findViewById(R.id.fab_lang);
//        viewPager = findViewById(R.id.view_pager);
//        viewPager.setAdapter(placeViewPagerAdapter);
//        viewPager.setPadding(0,0,0,0);
////
////
//        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//
//
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });
////
//        lan_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showChangeLanguageDialog();
//            }
//       });
//
//    }
//    private void showChangeLanguageDialog() {
//        final String[] lang_list = {"አማርኛ","English","Deutsche","हिंदी","Française","عربى","中文","Default"};
//        AlertDialog.Builder myBuilder = new AlertDialog.Builder(this);
//        myBuilder.setTitle("Choose Language");
//        myBuilder.setSingleChoiceItems(lang_list, -1, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                 if(which == 0){
//                     setLocal("am");
//                     recreate();
//                 }else if(which == 1){
//                     setLocal("en");
//                     recreate();
//                 }else if(which == 2){
//                     setLocal("de");
//                     recreate();
//                 }else if(which == 3){
//                     setLocal("hi");
//                     recreate();
//                 }else if(which == 4){
//                     setLocal("fr");
//                     recreate();
//                 }else if(which == 5){
//                     setLocal("ar");
//                     recreate();
//                 }else if(which == 6){
//                     setLocal("zh");
//                     recreate();
//                 }else if(which == 7){
//                     setLocal("am");
//                     recreate();
//                 }
//
//                 dialog.dismiss();
//
//            }
//        });
//        AlertDialog myDialog = myBuilder.create();
//        myDialog.show();
//    }
//
//    private void setLocal(String lang) {
//        Locale locale = new Locale(lang);
//        Locale.setDefault(locale);
//        Configuration configuration = new Configuration();
//        configuration.locale = locale;
//        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());
//        SharedPreferences.Editor editorForLang = getSharedPreferences("settings",MODE_PRIVATE).edit();
//        editorForLang.putString("my_Lang",lang);
//        editorForLang.apply();
//
//    }
//    public void loadLocale() {
//            SharedPreferences spForLang = getSharedPreferences("settings", Activity.MODE_PRIVATE);
//            String language = spForLang.getString("my_Lang", "");
//            setLocal(language);
//
//   }
    }
}
