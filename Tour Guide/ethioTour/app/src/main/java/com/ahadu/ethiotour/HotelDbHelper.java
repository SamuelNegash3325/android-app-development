package com.ahadu.ethiotour;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class HotelDbHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "EthioTourDB.db";
    public static final String Hotel_Table = "HotelsTable";
    public static final String hname = "HotelName";
    public static final String hcity = "City";




    public HotelDbHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String create_hotel_table = " CREATE TABLE "+Hotel_Table+
                "("
                +hname+ " varchar(100),"
                +hcity+ " varchar(100)"
                +")";
        db.execSQL(create_hotel_table);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if( oldVersion != newVersion){

            db.execSQL("DROP TABLE IF EXISTS "+ Hotel_Table);
            onCreate(db);
        }

    }
    public boolean insertData(String h,String c){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues= new ContentValues();
        contentValues.put(hname,h);
        contentValues.put(hcity,c);
        long result = db.insert(Hotel_Table,null,contentValues);
        if(result==-1){
            return false;
        }else{
            return true;
        }
}
    public Cursor getAllCitiesData(){
        SQLiteDatabase db =this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM "+Hotel_Table ,null);
        return cursor;
    }
}
