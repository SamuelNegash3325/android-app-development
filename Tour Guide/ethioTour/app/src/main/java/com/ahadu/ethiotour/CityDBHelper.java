package com.ahadu.ethiotour;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class CityDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "EthioTourDB.db";
    public static final String TABLE_NAME = "CitiesTable";
    public static final String Col_2 = "City_Name";
    public static final String COL_3 = "Region";
    public static final String COL_4 = "Population";
    public static final String COL_5 = "Whether";

    public CityDBHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String create_table = " CREATE TABLE "+TABLE_NAME+
                "("
                +Col_2+ " varchar(100),"
                +COL_3+ " varchar(100),"
                +COL_4+ " varchar(100),"
                +COL_5+ " varchar(100)"
                +")";
        db.execSQL(create_table);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if( oldVersion != newVersion){
            db.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME);
            onCreate(db);
        }
    }
    public boolean insert(String c,String r,String p,String w){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues= new ContentValues();
        contentValues.put(Col_2,c);
        contentValues.put(COL_3,r);
        contentValues.put(COL_4,p);
        contentValues.put(COL_5,w);
        long result = db.insert(TABLE_NAME,null,contentValues);
        if(result==-1){
            return false;
        }else{
            return true;
        }

    }

    public Cursor getAllCitiesData(){
        SQLiteDatabase db =this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM "+TABLE_NAME ,null);
        return cursor;
    }

}
