package com.ahadu.ethiotour;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.database.Cursor;
import android.os.Bundle;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ListActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    CityDBHelper db =new CityDBHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        recyclerView = findViewById(R.id.recycler);
        List<ListModel> listModels = new ArrayList<>();

        insert();
        Cursor cursor = db.getAllCitiesData();
        if(cursor.getCount() == 0){
            return;
        }
        String[] citieInfo = new String[4];
        while (cursor.moveToNext()){
            citieInfo[0] = cursor.getString(0);
            citieInfo[1] = cursor.getString(1);
            citieInfo[2] = cursor.getString(2);
            citieInfo[3] = cursor.getString(3);
            listModels.add(new ListModel(citieInfo[0],citieInfo[1],citieInfo[2],citieInfo[3]));

        }
        ListAdapter listAdapter = new ListAdapter(this,listModels);
        recyclerView.setAdapter(listAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
    public void insert(){
        db.insert("Mekele", "Tigray", "350000", "27 c");
        db.insert("Semera", "Afar", "150000", "33 c");
        db.insert("Bahir Dar", "Amhara", "350000", "27 c");
        db.insert("Addis Ababa", "Oromia", "5000000", "25 c");
        db.insert("Adama", "Oromia", "324000", "27 c");
        db.insert("Hawassa", "Sidama", "350000", "27 c");
        db.insert("Gambela", "Gambela", "175000", "33 c");
        db.insert("Harar", "Harari", "200000", "27 c");
        db.insert("Dire Dawa", "Oromia", "250000", "27 c");
        boolean r = db.insert("Asosa", "Benishangul Gumuz", "350000", "27 c");
        if(r){
            Toast.makeText(getApplicationContext(),"Successfully Saved",Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(getApplicationContext(),"unSuccessfully Saved",Toast.LENGTH_SHORT).show();

        }
    }


}
