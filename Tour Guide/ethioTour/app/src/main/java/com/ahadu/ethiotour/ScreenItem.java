package com.ahadu.ethiotour;

public class ScreenItem {
    String title,description;
    int ScreeImg;

    public ScreenItem(String title, String description, int screeImg) {
        this.title = title;
        this.description = description;
        ScreeImg = screeImg;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setScreeImg(int screeImg) {
        ScreeImg = screeImg;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getScreeImg() {
        return ScreeImg;
    }
}
