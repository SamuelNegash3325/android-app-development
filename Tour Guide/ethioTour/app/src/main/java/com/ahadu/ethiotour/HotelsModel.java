package com.ahadu.ethiotour;

public class HotelsModel {
    int hotelImage;
    String hotelName;

    public HotelsModel(int hotelImage, String hotelName) {
        this.hotelImage = hotelImage;
        this.hotelName = hotelName;
    }

    public int getHotelImage() {
        return hotelImage;
    }

    public String getHotelName() {
        return hotelName;
    }
}
