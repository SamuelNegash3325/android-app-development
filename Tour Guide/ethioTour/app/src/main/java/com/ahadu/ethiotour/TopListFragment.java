package com.ahadu.ethiotour;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class TopListFragment extends Fragment {

    HotelDbHelper db = new HotelDbHelper(getContext());
    List<PlaceModel> placeModels;
    List<HotelsModel> hotelsModels;
    RecyclerView recyclerViewForHotels,recyclerViewForPlaces;

    public TopListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_top_list, container, false);

        placeModels = new ArrayList<>();
        placeModels.add(new PlaceModel(R.mipmap.axum,getResources().getString(R.string.Axum),""));
        placeModels.add(new PlaceModel(R.mipmap.denakil,getResources().getString(R.string.Ert_Ale),""));
        placeModels.add(new PlaceModel(R.mipmap.fasil,getResources().getString(R.string.Fasil),""));
        placeModels.add(new PlaceModel(R.mipmap.harar,getResources().getString(R.string.Harar),""));
        placeModels.add(new PlaceModel(R.mipmap.hawassa,getResources().getString(R.string.Hawassa),""));
        placeModels.add(new PlaceModel(R.mipmap.gambela,getResources().getString(R.string.Gambela),""));
        placeModels.add(new PlaceModel(R.mipmap.lalibela,getResources().getString(R.string.Lalibela),""));
        placeModels.add(new PlaceModel(R.mipmap.land,getResources().getString(R.string.Land),""));
        placeModels.add(new PlaceModel(R.mipmap.tana,getResources().getString(R.string.Lake_Tana),""));



        //recycler for places

        recyclerViewForPlaces = v.findViewById(R.id.historical_recycler);//Assign the ReciclerView to recyclerView object by id
        LinearLayoutManager linearLayoutManagers = new LinearLayoutManager(getContext());//create layout manager
        linearLayoutManagers.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerViewForPlaces.setLayoutManager(linearLayoutManagers);//set the orientation of layout vertical
        PlaceAdapter placeViewPagerAdapter = new PlaceAdapter(placeModels,getContext());recyclerViewForPlaces.setLayoutManager(linearLayoutManagers);//give the layout we created before to the recyclerview object
        recyclerViewForPlaces.setAdapter(placeViewPagerAdapter);

        hotelsModels = new ArrayList<>();
        hotelsModels.add(new HotelsModel(R.mipmap.taye_belay,"Taye Belay Hotel"));
        hotelsModels.add(new HotelsModel(R.mipmap.lal_hotel_and_spa,"Lal Hotel and Spa"));
        hotelsModels.add(new HotelsModel(R.mipmap.benmas,"Benmas Hotel"));
        hotelsModels.add(new HotelsModel(R.mipmap.golden_tulip,"Golden Tulip Hotel"));
        hotelsModels.add(new HotelsModel(R.mipmap.harbe,"Harbe Hotel"));
        hotelsModels.add(new HotelsModel(R.mipmap.aby_minch,"Abay Minch Lodge"));
        hotelsModels.add(new HotelsModel(R.mipmap.sheraton,"Sheraton Hotel"));
        hotelsModels.add(new HotelsModel(R.mipmap.hilton,"Hilton Hotel"));
        hotelsModels.add(new HotelsModel(R.mipmap.sky_light,"Sky Light Hotel"));


//        insert();

        HotelAdapter hotelRecyclerAdapter = new HotelAdapter(getContext(),hotelsModels);
        recyclerViewForHotels = v.findViewById(R.id.hotel_recycler);//Assign the ReciclerView to recyclerView object by id
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());//create layout manager
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);//set the orientation of layout vertical
        recyclerViewForHotels.setLayoutManager(linearLayoutManager);//give the layout we created before to the recyclerview object
        recyclerViewForHotels.setAdapter(hotelRecyclerAdapter);



//        recyclerViewForPlaces.setPadding(0,0,0,0);

        return v;
    }
//    public void insert(){
//         db.insertData("Taye Belay Hotel","Bahir Dar");
//         db.insertData("Lal Hotel and Spa","Bahir Dar");
//         db.insertData("Benmas Hotel","Bahir Dar");
//         db.insertData("Golden Tulip Hotel","Addis Ababa");
//         db.insertData("Harbe Hotel","Bahir Dar");
//         db.insertData("Abay Minch Lodge","Bahir Dar");
//         db.insertData("Sheraton Hotel","Addis Ababa");
//         db.insertData("Hilton Hotel","Addiss Ababa");
//        boolean r = db.insertData("Sky Light Hotel","Addiss Ababa");
//        if(r){
//            Toast.makeText(getContext(),"Successfully Saved",Toast.LENGTH_SHORT).show();
//        }else{
//            Toast.makeText(getContext(),"unSuccessfully Saved",Toast.LENGTH_SHORT).show();
//
//        }
//
//
//    }

}
