package com.ahadu.ethiotour;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class introActivity extends AppCompatActivity {

    private ViewPager screenPager;
    IntroViewPagerAdapter introViewPagerAdapter;
    TabLayout tabIndicator;
    Button btnNext;
    int position = 0;
    Button btnGetStarted;
    Animation btnAnimation;
    Button skip_btn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

//        if(restorePrefData()){
//            Intent intent = new Intent(getApplicationContext(),MainActivity.class);
//            startActivity(intent);
//            finish();
//        }


        setContentView(R.layout.activity_intro);
//        getSupportActionBar().hide();



        skip_btn = findViewById(R.id.skip_btn);
        btnNext = findViewById(R.id.btn_next);
        btnGetStarted = findViewById(R.id.btn_get_started);
        tabIndicator = findViewById(R.id.tab_indicator);
        btnAnimation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.button_animation);

        final List<ScreenItem> items = new ArrayList<>();
        items.add(new ScreenItem("Places", "lorem ipsum texts is here the description of the title", R.mipmap.image1));
        items.add(new ScreenItem("Tours","lorem ipsum texts is here the description of the title",R.mipmap.image2));
        items.add(new ScreenItem("Guiding","lorem ipsum texts is here the description of the title",R.mipmap.image7));
        items.add(new ScreenItem("family","lorem ipsum texts is here the description of the title",R.mipmap.image4));
        items.add(new ScreenItem("Around earth","lorem ipsum texts is here the description of the title",R.mipmap.image11));
        items.add(new ScreenItem("Guiding","lorem ipsum texts is here the description of the title",R.mipmap.image10));
        items.add(new ScreenItem("family","lorem ipsum texts is here the description of the title",R.mipmap.imge8));

        screenPager = findViewById(R.id.screen_viewpager);
        introViewPagerAdapter = new IntroViewPagerAdapter(this,items);
        screenPager.setAdapter(introViewPagerAdapter);

        tabIndicator.setupWithViewPager(screenPager);


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                position = screenPager.getCurrentItem();
                if(position<items.size()){
                    position++;
                    screenPager.setCurrentItem(position);
                }
                if(position == items.size()-1){
                    loadLastScreen();
                }

            }
        });

        tabIndicator.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(tab.getPosition() ==items.size()-1){
                    loadLastScreen();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        btnGetStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),LandingPage.class);
                startActivity(intent);
//                savePrefsData();
                finish();
            }
        });
        skip_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screenPager.setCurrentItem(items.size()-1);
            }
        });
    }
//    private boolean restorePrefData() {
//
//        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("MyPref",MODE_PRIVATE);
//        Boolean isIntroActivityOpenedBefore = sharedPreferences.getBoolean("isIntroOpened",false);
//        return isIntroActivityOpenedBefore;
//    }
//
//    private void savePrefsData() {
//        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("MyPref",MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.putBoolean("isIntroOpened",true);
//        editor.commit();
//    }
    private void loadLastScreen() {
        btnNext.setVisibility(View.INVISIBLE);
        btnGetStarted.setVisibility(View.VISIBLE);
        tabIndicator.setVisibility(View.INVISIBLE);
        btnGetStarted.setAnimation(btnAnimation);
        skip_btn.setVisibility(View.INVISIBLE);
    }
}
