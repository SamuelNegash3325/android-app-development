package com.ahadu.ethiotour;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.viewholder> {
Context context;
List<ListModel> listModels;

    public ListAdapter(Context context, List<ListModel> listModels) {
        this.context = context;
        this.listModels = listModels;
    }

    @NonNull
    @Override
    public viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.listlayout,parent,false);
        return new viewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull viewholder holder, int position) {
        holder.itemView.setAnimation(AnimationUtils.loadAnimation(context,R.anim.list_animation));
        holder.container.setAnimation(AnimationUtils.loadAnimation(context,R.anim.scale));

        final String city= listModels.get(position).getCity();
        String region= listModels.get(position).getRegion();
        String population= listModels.get(position).getPopulation();
        String weather= listModels.get(position).getWeather();
        viewholder.setData(city,region,population,weather);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,DetailOfHistorical.class);
                 intent.putExtra("cityName",city);

                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return listModels.size();
    }
    public static class viewholder extends RecyclerView.ViewHolder{
        static  TextView city;
        static  TextView region;
        static  TextView population;
        static  TextView weather;
        LinearLayout container;
        public viewholder(@NonNull View itemView) {
            super(itemView);
            city = itemView.findViewById(R.id.city);
            region =itemView.findViewById(R.id.region);
            population =itemView.findViewById(R.id.population);
            weather = itemView.findViewById(R.id.weather);
            container = itemView.findViewById(R.id.container);


        }
        public  static void setData(String c,String r,String p,String w){
              city.setText(c);
              region.setText(r);
              population.setText(p);
              weather.setText(w);
        }
    }
}
