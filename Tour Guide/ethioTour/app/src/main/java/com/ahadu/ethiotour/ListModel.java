package com.ahadu.ethiotour;

public class ListModel {
    String city,region,population,weather;

    public String getCity() {
        return city;
    }

    public String getRegion() {
        return region;
    }

    public String getPopulation() {
        return population;
    }

    public String getWeather() {
        return weather;
    }

    public ListModel(String city, String region, String population, String weather) {
        this.city = city;
        this.region = region;
        this.population = population;
        this.weather = weather;
    }
}
