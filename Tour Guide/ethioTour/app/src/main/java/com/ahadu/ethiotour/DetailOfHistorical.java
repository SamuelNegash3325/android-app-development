package com.ahadu.ethiotour;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailOfHistorical extends AppCompatActivity {
   ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_of_historical);
        imageView = findViewById(R.id.img_view);
        String x;
        Intent intent = getIntent();
        x = intent.getStringExtra("cityName");

        if(x == "Bahir Dar"){
            imageView.setImageResource(R.mipmap.tana);
        }else if(x == "Gambela"){
            imageView.setImageResource(R.mipmap.gambela);
        }


    }
}
