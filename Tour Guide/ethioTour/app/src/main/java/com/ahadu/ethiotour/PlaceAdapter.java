package com.ahadu.ethiotour;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class PlaceAdapter extends RecyclerView.Adapter<PlaceAdapter.viewholder>{

    private Context context;
    private List<PlaceModel> placeModels;


    public PlaceAdapter(List<PlaceModel> placeModels, Context context) {
        this.placeModels = placeModels;
        this.context = context;
    }


    @NonNull
    @Override
    public viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(context).inflate(R.layout.place_layout,parent,false);

        return new viewholder(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull viewholder holder, int position) {
        int img = placeModels.get(position).getImage();
        final String title = placeModels.get(position).getTitle();
        String desc = placeModels.get(position).getDesc();

        viewholder.setData(img,title,desc);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,DetailOfHistorical.class);
                intent.putExtra("key",title);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
         return placeModels.size();
    }
    public static class viewholder extends RecyclerView.ViewHolder{
        static ImageView img;
        static TextView txt;
        static TextView descr;

        public viewholder(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.place_image);
            txt = itemView.findViewById(R.id.place_name);
            descr = itemView.findViewById(R.id.place_desc);
        }
        public static void setData(int image,String name,String desc){
            img.setImageResource(image);
            txt.setText(name);
            descr.setText(desc);
        }
    }
}
