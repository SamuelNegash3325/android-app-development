package com.ahadu.ethiotour;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class HotelAdapter extends RecyclerView.Adapter<HotelAdapter.viewholder> {
     Context context;
     List<HotelsModel> hotelsModels;

    public HotelAdapter(Context context, List<HotelsModel> hotelsModels) {
        this.context = context;
        this.hotelsModels = hotelsModels;
    }

    @NonNull
    @Override
    public viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.hotel_layout,parent,false);
        return new viewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull viewholder holder, int position) {
        int img = hotelsModels.get(position).getHotelImage();
        String title = hotelsModels.get(position).getHotelName();

        viewholder.setData(img,title);
    }

    @Override
    public int getItemCount() {
        return hotelsModels.size();
    }

    public static class viewholder extends RecyclerView.ViewHolder{
        static ImageView image;
        static TextView tt;
        public viewholder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.hotel_image);
            tt = itemView.findViewById(R.id.hotel_title);



        }
        public static void setData(int img,String title){
            image.setImageResource(img);
            tt.setText(title);
        }
    }
}
